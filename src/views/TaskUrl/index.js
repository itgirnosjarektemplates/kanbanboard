import React, { Component } from 'react';
import '../../App.css';
import './index.css';
import RegularButton from "../../components/Form/RegularButton";
import {formatDate} from "../../services/generalServices";
import {deleteTask, setBoardData} from "../../state/actions/board";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {INITIAL_STATE} from "../../state/constant";

const customStyles = {
    marginLeft: {
        marginLeft: 20
    }
};

class TaskUrl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedColumn: {
                id: '',
                title: ''
            },
            selectedTask: {
                createdAt: '',
                editedAt: ''
            }
        };
    }

    componentDidMount() {
        const { board, setBoardData } = this.props;
        const data = JSON.parse(localStorage.getItem('board'));
        if (!data) {
            localStorage.setItem('board', JSON.stringify(INITIAL_STATE));
            return;
        }
        if (data.columns && !board.columns.length) {
            setBoardData(data);
        }

        const taskId = this.props.match.params.id;
        if(data && Object.keys(data.tasks).length && taskId){
            Object.keys(data.tasks).map(id => {
                if(taskId === id){
                    this.setState({selectedTask: data.tasks[id]});
                }
            });
            if(Object.keys(data.columns).length){
                Object.keys(data.columns).map(colId => {
                    if(data.columns[colId].taskIds.includes(taskId)){
                        this.setState({selectedColumn: data.columns[colId]});
                    }
                });
            }
        }
    }

    deleteTask = (taskId, columnId) => {
        this.props.deleteTask({taskId, columnId});
        alert('The task was successfully deleted!');
        this.props.history.push('/');
    };

    render() {
        const {selectedTask, selectedColumn} = this.state;
        return (
            <div className="app">
                <div className="task-url">
                    <div className="row">
                        <p><b>Column:</b> {selectedColumn.title}</p>
                    </div>
                    <div className="row">
                        <p><b>Task title:</b> {selectedTask.title}</p>
                    </div>
                    <div className="row">
                        <p><b>Task description:</b></p>
                    </div>
                    <div className="row">
                        <p>{selectedTask.description}</p>
                    </div>
                    <div className="row">
                        <p><b>Created at:</b> {selectedTask.createdAt ? formatDate(selectedTask.createdAt) : 'Task was not created'}</p>
                    </div>
                    <div className="row">
                        <p><b>Edited at:</b> {selectedTask.editedAt ? formatDate(selectedTask.editedAt) : 'Task was not edited'}</p>
                    </div>
                    <div className="row">
                        <RegularButton
                            onClick={() => this.deleteTask(selectedTask.id, selectedColumn.id)}
                            title={'Delete task'}
                        />
                        <RegularButton
                            style={customStyles.marginLeft}
                            onClick={() => this.props.history.push('/')}
                            title={'Back to board'}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const enhance = connect(
    state => ({
        board: state.board,
    }),
    dispatch =>
        bindActionCreators(
            {deleteTask: deleteTask, setBoardData: setBoardData},
            dispatch
        )
);

export default enhance(TaskUrl);
