import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';
import './index.css';

class Task extends Component {
    render() {
        const { task, column, index, openTaskModalEdit } = this.props;

        if (task) {
            return (
                <Draggable draggableId={task.id} index={index}>
                    {(provided, snapshot) => (
                        <div className="task"
                             onClick={() => openTaskModalEdit(column, task)}
                             {...provided.draggableProps}
                             {...provided.dragHandleProps}
                             ref={provided.innerRef}
                        >{task.title}</div>
                    )}
                </Draggable>
            );
        }
        return '';
    }
}

export default Task;
