import React, { Component } from 'react';
import Task from '../Task';
import {Droppable} from "react-beautiful-dnd";
import RegularButton from "../../../components/Form/RegularButton";
import './index.css';

class Column extends Component {
    render() {
        const { column, tasks, openTaskModal, openTaskModalEdit, openColumnModalEdit} = this.props;
        return(
            <div className="column">
                <div className="column-control">
                    <RegularButton
                        onClick={openTaskModal}
                        title={'ADD TASK'}
                    />
                    <RegularButton
                        onClick={openColumnModalEdit}
                        title={'EDIT COLUMN'}
                    />
                </div>
                <div className="title">{column.title}</div>
                <Droppable droppableId={column.id}>
                    {(provided, snapshot) => (
                        <div className="task-container"
                             ref={provided.innerRef}
                             {...provided.droppableProps}
                        >
                            {tasks && tasks.map((task, index) => {
                                return (
                                    <Task
                                        openTaskModalEdit={openTaskModalEdit}
                                        column={column}
                                        key={task.id}
                                        task={task}
                                        index={index}
                                    />
                                );
                            })}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </div>
        );
    }
}

export default Column;
