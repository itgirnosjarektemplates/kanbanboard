import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    addColumn,
    editColumn,
    deleteColumn,
    addTask,
    editTask,
    deleteTask,
    changeTaskOrder,
    changeTaskColumn,
    setBoardData
} from '../../state/actions/board';
import {INITIAL_STATE} from '../../state/constant';
import TaskModal from "../../components/Modals/TaskModal";
import ColumnModal from "../../components/Modals/ColumnModal";
import RegularButton from "../../components/Form/RegularButton";
import Column from "./Column";
import '../../App.css';
import './index.css';
import {DragDropContext} from "react-beautiful-dnd";

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedColumn: {
                id: '',
                title: ''
            },
            selectedTask: {
                id: '',
                title: '',
                description: ''
            },
            columnModal: false,
            columnCreate: false,
            columnEdit: false,
            taskModal: false,
            taskCreate: false,
            taskEdit: false
        };
    }

    componentDidMount() {
        const { board, setBoardData } = this.props;
        const data = JSON.parse(localStorage.getItem('board'));
        if (!data) {
            localStorage.setItem('board', JSON.stringify(INITIAL_STATE));
            return;
        }
        if (data.columns && !board.columns.length) {
            setBoardData(data);
        }
    };

    setInitState = () => {
        this.setState({
            selectedColumn: {
                id: '',
                title: ''
            },
            selectedTask: {
                id: '',
                title: '',
                description: ''
            },
            columnModal: false,
            columnCreate: false,
            columnEdit: false,
            taskModal: false,
            taskCreate: false,
            taskEdit: false
        });
    };

    addColumn = () => {
        const { addColumn, board } = this.props;
        let lastColumnId;
        if(Object.keys(board.columns).length){
            lastColumnId = Object.keys(board.columns).length + 1;
        }else{
            lastColumnId = 1;
        }
        const column = {
            ['column-' + lastColumnId]: {
                id: 'column-' + lastColumnId,
                title: this.state.selectedColumn.title,
                taskIds: []
            }
        };
        addColumn(column);
        this.setInitState();
    };

    editColumn = (column) => {
        this.props.editColumn(column);
        this.setInitState();
    };

    deleteColumn = (column) => {
        this.props.deleteColumn(column);
        this.setInitState();
    };

    createTask = (column) => {
        let lastTaskId;
        if(Object.keys(this.props.board.tasks).length){
            lastTaskId = Object.keys(this.props.board.tasks).length + 1;
        }else{
            lastTaskId = 1;
        }
        this.props.addTask({
            lastTaskId: 'task-'+lastTaskId,
            task: {
                ['task-'+lastTaskId]: {
                    id: 'task-'+lastTaskId,
                    title: this.state.selectedTask.title,
                    description: this.state.selectedTask.description
                }},
            column: column
        });
        this.setInitState();
    };

    editTask = task => {
        this.props.editTask(task);
        this.setInitState();
    };

    deleteTaskFromColumn = (taskId, columnId) => {
        this.props.deleteTask({taskId, columnId});
        this.setInitState();
    };

    onDragEnd = result => {
        const {board, changeTaskOrder, changeTaskColumn} = this.props;
        const {destination, source, draggableId} = result;
        if(!destination){
            return;
        }
        if(destination.droppableId === source.droppableId && destination.index === source.index){
            return;
        }
        const oldCol = board.columns[source.droppableId];
        const newCol = board.columns[destination.droppableId];
        if (newCol === oldCol) {
            const newTaskIds = Array.from(oldCol.taskIds);
            newTaskIds.splice(source.index, 1);
            newTaskIds.splice(destination.index, 0, draggableId);

            const newColumn = {
                ...oldCol,
                taskIds: newTaskIds,
            };

            changeTaskOrder(newColumn);
        }else{
            const oldColTaksIds = Array.from(oldCol.taskIds);
            oldColTaksIds.splice(source.index, 1);

            const newOldCol = {
                ...oldCol,
                taskIds: oldColTaksIds
            };

            const newColTaskIds = Array.from(newCol.taskIds);
            newColTaskIds.splice(destination.index, 0, draggableId);

            const newNewCol = {
                ...newCol,
                taskIds: newColTaskIds
            };

            changeTaskColumn({newOldCol, newNewCol});
        }
    };

    render() {
        const {board} = this.props;
        const {taskCreate, taskModal, columnCreate, selectedColumn, selectedTask, columnModal, columnEdit} = this.state;

        return (
            <div className="app">
                <div className="header">
                    <RegularButton
                        onClick={() => this.setState({columnModal: true, columnCreate: true})}
                        title={'ADD COLUMN'}
                    />
                </div>
                <div className="board">
                    <div className="columns">
                        <DragDropContext onDragEnd={this.onDragEnd}>
                            {board && board.columnOrder.map(columnId => {
                                const column = board.columns[columnId];
                                if(!column){
                                    return '';
                                }
                                const tasks = column.taskIds.map(taskId => board.tasks[taskId]);
                                return (
                                    <Column
                                        openTaskModal={() => this.setState({
                                            taskCreate: true,
                                            taskModal: true,
                                            taskEdit: false,
                                            selectedColumn: column
                                        })}
                                        openTaskModalEdit={(column, task) => this.setState({
                                            taskCreate: false,
                                            taskModal: true,
                                            taskEdit: true,
                                            selectedColumn: column,
                                            selectedTask: task
                                        })}
                                        openColumnModalEdit={() => this.setState({
                                            columnModal: true,
                                            columnEdit: true,
                                            columnCreate: false,
                                            selectedColumn: column,
                                        })}
                                        key={column.id}
                                        column={column}
                                        tasks={tasks}
                                    />
                                );
                            })}
                        </DragDropContext>
                    </div>
                </div>

                <ColumnModal
                    isOpen={columnModal}
                    onRequestClose={this.setInitState}
                    title={columnCreate ? 'Create column' : 'Column edit'}
                    titleInput={{
                        'name': 'title',
                        'value': selectedColumn.title,
                        'label': 'Title',
                        'onChange': e => {
                            const target = e.target;
                            this.setState(prevState => ({
                                ...prevState,
                                selectedColumn: {
                                    ...prevState.selectedColumn,
                                    [target.name]: target.value
                                }
                            }))
                        }
                    }}
                    buttons={{
                        'approve': {
                            'title': 'Save',
                            'action': () => columnCreate ? this.addColumn() : this.editColumn(selectedColumn)
                        },
                        'cancel': {
                            'title': 'Close',
                            'action': this.setInitState
                        },
                        'deleteCol': {
                            'title': 'Delete',
                            'hide': columnEdit,
                            'action': () => this.deleteColumn(selectedColumn)
                        }
                    }}
                />

                <TaskModal
                    isOpen={taskModal}
                    onRequestClose={this.setInitState}
                    title={taskCreate ? 'Create task' : 'Edit task'}
                    titleInput={{
                        'name': 'title',
                        'value': selectedTask.title,
                        'label': 'Title',
                        'onChange': e => {
                            const target = e.target;
                            this.setState(prevState => ({
                                ...prevState,
                                selectedTask: {
                                    ...prevState.selectedTask,
                                    [target.name]: target.value
                                }
                            }))
                        }
                    }}
                    descriptionInput={{
                        'descName': 'description',
                        'descValue': selectedTask.description,
                        'descLabel': 'Description',
                        'descOnChange': e => {
                            const target = e.target;
                            this.setState(prevState => ({
                                ...prevState,
                                selectedTask: {
                                    ...prevState.selectedTask,
                                    [target.name]: target.value
                                }
                            }))
                        }
                    }}
                    buttons={{
                        'approve': {
                            'title': 'Save',
                            'action': () => taskCreate ? this.createTask(selectedColumn) : this.editTask(selectedTask)
                        },
                        'cancel': {
                            'title': 'Close',
                            'action': this.setInitState
                        },
                        'deleteTask': {
                            'title': 'Delete',
                            'hide': !taskCreate,
                            'action': () => this.deleteTaskFromColumn(selectedTask.id, selectedColumn.id)
                        },
                        'goToTask': {
                            'title': 'Go to task',
                            'hide': !taskCreate,
                            'action': () => this.props.history.push(`/task/${selectedTask.id}`)
                        }
                    }}
                />
            </div>
        );
    }
}

const enhance = connect(
    state => ({
        board: state.board,
    }),
    dispatch =>
        bindActionCreators(
            {
                addColumn: addColumn,
                editColumn: editColumn,
                deleteColumn: deleteColumn,
                addTask: addTask,
                editTask: editTask,
                deleteTask: deleteTask,
                changeTaskOrder: changeTaskOrder,
                changeTaskColumn: changeTaskColumn,
                setBoardData: setBoardData
            },
            dispatch
        )
);

export default enhance(Dashboard);