// Format Date
export const formatDate = function (date) {
    let d = new Date(date),
        seconds = '' + d.getSeconds(),
        minutes = '' + d.getMinutes(),
        hours = '' + d.getHours(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hours.length < 2) hours = '0' + hours;
    if (minutes.length < 2) minutes = '0' + minutes;
    if (seconds.length < 2) seconds = '0' + seconds;

    let arr1 = [year, month, day].join('-');
    let arr2 = [hours, minutes, seconds].join(':');

    return arr1 + ' ' + arr2;
};

// Delete object from object
export const deleteObjectFromObject = function (state, objKey) {
    Object.keys(state).forEach(item => {
        if(item === objKey){
            delete state[objKey];
        }
    });
    return state;
};

// Delete tasks of selected column
export const deleteTasks = function (tasks, taskIds) {
    if(!taskIds.length){
        return tasks;
    }
    if(Object.entries(tasks).length === 0 && tasks.constructor === Object){
        return tasks;
    }
    Object.keys(tasks).forEach(item => {
        if(taskIds.includes(item)){
            delete tasks[item];
        }
    });
    return tasks;
};

// Delete task
export const deleteTask = function (tasks, taskId) {
    if(!Object.keys(tasks).length){
        return tasks;
    }
    if(tasks[taskId]){
        delete tasks[taskId];
    }
    return tasks;
};