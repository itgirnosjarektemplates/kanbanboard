import React from 'react';
import Modal from './index';
import RegularButton from '../../components/Form/RegularButton';
import RegularInput from '../../components/Form/RegularInput';

const customStyles = {
    modalContentContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    centerRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        width: '100%'
    },
    marginLeft: {
        marginLeft: 20
    }
};

export default props => {
    const {
        isOpen,
        onRequestClose,
        title,
        titleInput: { value, name, onChange, label },
        buttons: { approve, cancel, deleteCol }
    } = props;

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
        >
            <div style={customStyles.modalContentContainer}>
                <h2>{title}</h2>
                <RegularInput
                    value={value || ''}
                    inputData={{ name, label }}
                    onChange={onChange}
                />
                <div style={customStyles.centerRow}>
                    <RegularButton
                        onClick={approve.action}
                        title={approve.title}
                    />
                    <RegularButton
                        style={customStyles.marginLeft}
                        onClick={cancel.action}
                        title={cancel.title}
                    />
                    {deleteCol.hide &&
                    <RegularButton
                        style={customStyles.marginLeft}
                        onClick={deleteCol.action}
                        title={deleteCol.title}
                    />}
                </div>
            </div>
        </Modal>
    )
};