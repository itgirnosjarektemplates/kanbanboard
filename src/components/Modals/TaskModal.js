import React from 'react';
import Modal from './index';
import RegularButton from '../../components/Form/RegularButton';
import RegularInput from '../../components/Form/RegularInput';
import TextArea from '../../components/Form/TextArea';

const customStyles = {
    modalContentContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    centerRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        width: '100%'
    },
    marginLeft: {
        marginLeft: 20
    }
};

export default props => {
    const {
        isOpen,
        onRequestClose,
        title,
        titleInput: { value, name, onChange, label },
        descriptionInput: { descValue, descName, descOnChange, descLabel },
        buttons: { approve, cancel, goToTask, deleteTask }
    } = props;

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
        >
            <div style={customStyles.modalContentContainer}>
                <h2>{title}</h2>
                <RegularInput
                    value={value}
                    inputData={{ name, label }}
                    onChange={onChange}
                />
                <TextArea
                    rows={5}
                    areaValue={descValue}
                    areaName={descName}
                    areaLabel={descLabel}
                    areaOnChange={descOnChange}
                />
                <div style={customStyles.centerRow}>
                    <RegularButton
                        onClick={approve.action}
                        title={approve.title}
                    />
                    <RegularButton
                        style={customStyles.marginLeft}
                        onClick={cancel.action}
                        title={cancel.title}
                    />
                    {deleteTask.hide &&
                    <RegularButton
                        style={customStyles.marginLeft}
                        onClick={deleteTask.action}
                        title={deleteTask.title}
                    />}
                    {goToTask.hide &&
                    <RegularButton
                        style={customStyles.marginLeft}
                        onClick={goToTask.action}
                        title={goToTask.title}
                    />}
                </div>
            </div>
        </Modal>
    )
};