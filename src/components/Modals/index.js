import React from 'react';
import Modal from 'react-modal';

const customStyles = {
    overlay: {
        backgroundColor: 'rgba(0,0,0, 0.4)'
    },
    content: {
        width: '400px',
        height: '340px',
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        padding: 40,
        transform: 'translate(-50%, -50%)'
    }
};

// Add this class (.animated) to the targeted UI container on the page
Modal.setAppElement('.animated');

export default props => {
    const { isOpen, onRequestClose, style } = props;

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={style ? style : customStyles}
        >
            {props.children}
        </Modal>
    )
};    
