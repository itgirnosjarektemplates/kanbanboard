import React from 'react';
import '../../App.css';

export default props => {
    const { inputData: { type, label, name }, value, onChange, disabled } = props;

    const inputType = type ? type : 'text';
    const inputLabel = label ? `${label}:` : undefined;

    return (
        <div className="col">
            {inputLabel && <label>{inputLabel}</label>}
            <input
                className="input"
                name={name}
                type={inputType}
                value={value}
                onChange={onChange}
                disabled={disabled}
            />
        </div>
    )
};    
