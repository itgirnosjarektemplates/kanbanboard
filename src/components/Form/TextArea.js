import React from 'react';

export default props => {
    const { areaLabel, areaName , areaValue, areaOnChange, areaDisabled, rows } = props;

    const inputLabel = areaLabel ? `${areaLabel}:` : undefined;

    return (
        <div className="col">
            {inputLabel && <label>{inputLabel}</label>}
            <textarea
                rows={rows ? rows : 4}
                className="input"
                name={areaName}
                value={areaValue}
                onChange={areaOnChange}
                disabled={areaDisabled}
            />
        </div>
    )
};    
