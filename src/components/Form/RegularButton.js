import React from 'react';

export default props => {
    const { onClick, title, disabled, btnClass, style } = props;

    return (
        <button
            type="button"
            className={btnClass ? btnClass : 'btn-blue'}
            disabled={disabled}
            onClick={onClick}
            style={style}
        >
            {title}
        </button>
    )

};