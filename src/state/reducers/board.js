import {INITIAL_STATE} from "../constant";
import {deleteObjectFromObject, deleteTasks, deleteTask} from "../../services/generalServices";

const boardReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case 'SET_BOARD_DATA':
            return { ...payload };
        case 'ADD_COLUMN':
            return {
                ...state,
                columns: { ...state.columns, ...payload },
                columnOrder: [...state.columnOrder, Object.keys(payload)[0]],
            };
        case 'EDIT_COLUMN':
            return {
                ...state,
                columns: { ...state.columns, [payload.id]: { ...payload } },
            };
        case 'DELETE_COLUMN':
            return {
                ...state,
                tasks: deleteTasks(state.tasks, payload.taskIds),
                columns: deleteObjectFromObject(state.columns, payload.id),
                columnOrder: state.columnOrder.filter(i => i !== payload.id)
            };
        case 'ADD_TASK':
            return {
                ...state,
                tasks: { ...state.tasks, ...payload.task },
                columns: {
                    ...state.columns,
                    [payload.column.id]: {
                        ...state.columns[payload.column.id],
                        taskIds: [
                            ...state.columns[payload.column.id].taskIds,
                            payload.lastTaskId
                        ],
                    },
                },
            };
        case 'EDIT_TASK':
            return {
                ...state,
                tasks: {
                    ...state.tasks,
                    [payload.id]: { ...payload },
                },
            };
        case 'DELETE_TASK':
            return {
                ...state,
                tasks: deleteTask(state.tasks, payload.taskId),
                columns: {
                    ...state.columns,
                    [payload.columnId]: {
                        ...state.columns[payload.columnId],
                        taskIds: state.columns[payload.columnId].taskIds.filter(t => t !== payload.taskId)
                    }
                }
            };
        case 'CHANGE_TASK_ORDER':
            return {
                ...state,
                columns: { ...state.columns, [payload.id]: { ...payload } },
            };
        case 'CHANGE_TASK_COLUMN':
            return {
                ...state,
                columns: {
                    ...state.columns,
                    [payload.newOldCol.id]: { ...payload.newOldCol },
                    [payload.newNewCol.id]: { ...payload.newNewCol },
                },
            };
        default:
            return state;
    }
};
export default boardReducer;