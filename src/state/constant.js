export const INITIAL_STATE = {
    columns: {},
    tasks: {},
    columnOrder: []
};

export const LS_ACTIONS = [
    'ADD_COLUMN',
    'EDIT_COLUMN',
    'DELETE_COLUMN',
    'ADD_TASK',
    'EDIT_TASK',
    'DELETE_TASK',
    'CHANGE_TASK_ORDER',
    'CHANGE_TASK_COLUMN'
];