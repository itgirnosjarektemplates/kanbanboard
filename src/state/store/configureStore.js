import LS from '../middlewares/localStorageMid';
import {applyMiddleware, createStore} from "redux";
import logger from 'redux-logger'
import allReducers from '../reducers';

export default function configureStore(preloadedState) {
    const middlewares = [logger, LS];
    const middlewareEnhancer = applyMiddleware(...middlewares);

    return createStore(allReducers, preloadedState, middlewareEnhancer);
}