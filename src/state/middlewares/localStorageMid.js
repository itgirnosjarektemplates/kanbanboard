import {LS_ACTIONS} from "../constant";

const LS = store => next => action => {
    const result = next(action);
    if(LS_ACTIONS.includes(action.type)){
        localStorage.setItem('board', JSON.stringify(store.getState().board));
    }
    return result;
};

export default LS;