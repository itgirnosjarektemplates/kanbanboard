export const addColumn = (payload) => {
    return {
        type: 'ADD_COLUMN',
        payload: payload
    }
};
export const editColumn = (payload) => {
    return {
        type: 'EDIT_COLUMN',
        payload: payload
    }
};
export const deleteColumn = (payload) => {
    return {
        type: 'DELETE_COLUMN',
        payload: payload
    }
};
export const addTask = (payload) => {
    return {
        type: 'ADD_TASK',
        payload: payload
    }
};
export const editTask = (payload) => {
    return {
        type: 'EDIT_TASK',
        payload: payload
    }
};
export const deleteTask = (payload) => {
    return {
        type: 'DELETE_TASK',
        payload: payload
    }
};
export const changeTaskOrder = (payload) => {
    return {
        type: 'CHANGE_TASK_ORDER',
        payload: payload
    }
};
export const changeTaskColumn = (payload) => {
    return {
        type: 'CHANGE_TASK_COLUMN',
        payload: payload
    }
};
export const setBoardData = (payload) => {
    return {
        type: 'SET_BOARD_DATA',
        payload: payload
    }
};