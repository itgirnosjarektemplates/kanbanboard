import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const Dashboard = Loadable({
    loader: () => import('./views/Dashboard'),
    loading
});

const TaskUrl = Loadable({
    loader: () => import('./views/TaskUrl'),
    loading
});

class App extends Component {

    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route path="/task/:id" name="TaskUrl" component={TaskUrl} />
                    <Route path="/" name="Dashboard" component={Dashboard} />
                </Switch>
            </HashRouter>
        );
    }
}

export default App;